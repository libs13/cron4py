# Тесты всех вохможных операторов в частях (отдельно и вместе)
# Граничные тесты (min.max)
import unittest
from datetime import timedelta

from cron4py.cron import Cron
from tests.data.days import DAYS
from tests.data.daysoftheweek import DAYSOFTHEWEEK
from tests.data.hours import HOURS
from tests.data.minutes import MINUTES
from tests.data.months import MONTHS
from tests.data.seconds import SECONDS
from tests.data.years import YEARS


class MainTests(unittest.TestCase):

    def run_func(self, cron_obj, result, notin):
        """Функция выполняющая генерацию дат"""
        cron_result = []
        for res in cron_obj.generator():
            cron_result.append(res)
        print(cron_result)
        print('Ожидание-Реальность: ', result - set(cron_result))
        self.assertEqual(len(result - set(cron_result)), 0)
        if len(notin - set(cron_result)) != len(notin):
            print(notin & set(cron_result))
            self.assertEqual(len(notin - set(cron_result)), len(notin))

    def autotest(self, data):
        """Функция выполняющая тестирование наборов данных"""
        for test in data:
            cron = test['cron']
            start, end = test['start_end']
            result = test['result']
            diff = (end - start).seconds
            notin = {(start + timedelta(seconds=i)) for i in range(diff + 1)} - result

            cron_obj = Cron(cron=cron, start=start, end=end, with_second=True)
            print(cron)
            self.run_func(cron_obj, result, notin)
            print('=' * 15, end='\n\n\n')

    def test_second(self):
        """Тестирование всех вохможных вариантов задания секунд"""
        self.autotest(SECONDS)

    def test_minute(self):
        """Тестирование всех вохможных вариантов задания минут"""
        self.autotest(MINUTES)

    def test_hour(self):
        """Тестирование всех вохможных вариантов задания часов"""
        self.autotest(HOURS)

    def test_day(self):
        """Тестирование всех вохможных вариантов задания дней"""
        self.autotest(DAYS)

    def test_month(self):
        """Тестирование всех вохможных вариантов задания месяцев"""
        self.autotest(MONTHS)

    def test_day_of_week(self):
        """Тестирование всех вохможных вариантов задания дней недели"""
        self.autotest(DAYSOFTHEWEEK)

    def test_year(self):
        """Тестирование всех вохможных вариантов задания лет"""
        self.autotest(YEARS)

    # def test_mergers(self):
    #     """Тестирование всех вохможных вариантов слияния частей"""
    #     self.autotest()
