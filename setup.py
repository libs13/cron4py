from setuptools import setup

setup(
    name='cron4py',
    version='1.0.6',
    description='Cron to list datetime',
    packages=['cron4py'],
    author_email='kivdev@yandex.ru',
    zip_safe=False
)
